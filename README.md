![ENVFULL][logo] ENVFULL
======

[![Pipeline][pipeline]][link-pipeline] 
[![Npm][npm-version]][link-npm]
[![Downloads][npm-downloads]][link-npm]
[![License][license]][link-license]
[![Node][node]][link-node]
[![Collaborators][collaborators]][link-npm]

### What is it?

This module is simple, no dependencies environment and cli parser used for parsing arguments from 
 commands, properties from config and variables from environment. Envfull loads data from this 
 sources and merge it together into one result.
 
 1. Load variables from `.env` file and enrich this data from `process.env`. This will create 
  object with environmental variables loaded from file and from environment.
 1. Load data from config (if you provided path to config). This will create object with variables
  from your config. Actual supported configs are: **`.json`**
 1. Load data from command line arguments and create object with variables and rest of items on
  command line.
 1. Merge these data with this order (env, config, command line) into one object, enrich this 
  object by defined defaults values and return it.
  
### Install

With [npm](https://npmjs.org) do:

```
npm install envfull
```
  
##### Types and types resolving

Envfull uses behaviour of javascript and try to conver values into typed values. So when there is 
 `9999` in variables, it will be typed as `Number`. If here is `true` or `false` it will be typed 
 as boolean. Same this has happend with command line arguments with same name. If you use 
 `--only 1 --only 2` Envfull make results as array `[1, 2]`. But if you use only `--only 1` variable
 will be presented as number `1`. Because of it, you can **force force typed on same keys**. More
 info you can read below in section [Options - forced types](#options---forced-types)
  
### Examples

> **You can try [ENVFULL playground on repl.it](https://repl.it/@stanislavhacker/Envfull-testing-playground)!**
>
> Test envfull and check if you can use it in your app. Live example with predefined command line arguments can
> help you too understand how envfull is working.

![replit][replit]

> More [examples can be found here](https://gitlab.com/stanislavhacker/envfull/blob/master/examples/index.md)! And if you want to
> add new one, just make and merge request and add your example to help others.

##### Typescript usage example

```typescript
import {envfull} from "envfull";
//without any settings
const parsed = envfull(process)();
```
```typescript
import {envfull} from "envfull";
//calling with json config
const parsed = envfull(process)("/path/to/config/config.json");
```
```typescript
import {envfull} from "envfull";
//with basic settings
type Settings = {
	db: {
		host: string;
		port: number;
	};
	production: boolean;
	user: {
		login: string | null;
	}
}
const parsed = envfull<Settings>(process, {
    //defaults vales
	defaults: {
		db: {
			host: "http://localhost",
			port: 9999
		},
		production: false
	},
	//alias for fields
	aliases: {
		db: {
			host: ["host"],
			port: ["port"]
		},
		production: ["p"],
		"user.login": ["username"]
	}
})();
```
```typescript
console.log(parsed.$); //Object tree with variables, merged from environmental variables also
console.log(parsed._); //Rest of items on command-line
console.log(parsed["--"]); //No parsed data after -- in command line
console.log(parsed.config.used); //Boolean that says if config file is loaded and used
console.log(parsed.config.path); //Path for used config file, if config is not used, there is empty string
console.log(parsed.env.used); //Boolean that says if .env file is loaded and used
console.log(parsed.env.path); //Path for used .env file, path is filled always even if env file not used
```

##### Javascript usage example

```javascript
const envfull = require("envfull").envfull;
const parsed = envfull(process)();
```
```typescript
const envfull = require("envfull").envfull;
//calling with json config
const parsed = envfull(process)("/path/to/config/config.json");
```
```javascript
const envfull = require("envfull").envfull;
//with basic settings
const parsed = envfull(process, {
    //defaults vales
	defaults: {
		db: {
			host: "http://localhost",
			port: 9999
		},
		production: false
	},
	//alias for fields
	aliases: {
		db: {
			host: ["host"],
			port: ["port"]
		},
		production: ["p"],
		"user.login": ["username"]
	}
})();
```
```javascript
console.log(parsed.$); //Object tree with variables, merged from environmental variables also
console.log(parsed._); //Rest of items on command-line
console.log(parsed["--"]); //No parsed data after -- in command line
console.log(parsed.config.used); //Boolean that says if config file is loaded and used
console.log(parsed.config.path); //Path for used config file, if config is not used, there is empty string
console.log(parsed.env.used); //Boolean that says if .env file is loaded and used
console.log(parsed.env.path); //Path for used .env file, path is filled always even if env file not used
```

### Options

```typescript
import {envfull} from "envfull";
const parsed = envfull(process, opts = {})();
```

#### opts.env: Array\<string | RegExp\>

List of all variables or regular expresion for variables name that will be import 
 from ENV store on NodeJS. *For example* if you will be used only "APP.USERNAME" and "APP.TOKEN"
 in you app, you can use 
```javascript
const opts = {
	env: ["APP.USERNAME", "APP.TOKEN"]
}
```

or with using RegExp 

```javascript
const opts = {
	env: [/APP\.[A-Za-z0-9]/]
}
```
Envfull will load only these variables from ENV store and rest will be ignored. **By default 
all ENV variables are imported.**
 
#### opts.aliases: Partial\<T\> | Object

This are aliases for some variables. Aliases are defined by partial object structure or you can
 define it like object mapping where levels are split by `.`.
 
Sou you can defined aliases by
```javascript
const opts = {
	aliases: {
		db: {
			host: ["APP.DB.HOST"],
 			port: ["APP.DB.PORT"]
		}       
	}
}
```

or you can use string maps version of aliases

```javascript
const opts = {
	aliases: {
		"db.host": ["APP.DB.HOST"],
		"db.port": ["APP.DB.PORT"]
	}
}
```

Both are valid settings, but if you use typescript, first version is recommended, because typescript
 will be showing properties of object.
 
#### opts.defaults: Partial<T> | Object

Default values for variables that are missing. If you not specified variables in command line, config 
 or in ENVIRONMENTS variables, envfull will use this default and fill results objects. Defaults are 
 defined by partial object structure or you can define it like object mapping where levels are 
 split by `.`.
 
 Sou you can defined defaults by
 ```javascript
 const opts = {
 	defaults: {
         db: {
             host: "http://localhost",
             port: 3333
         }       
 	}
 }
 ```
 
 or you can use string maps version of defaults
 
 ```javascript
 const opts = {
 	defaults: {
         "db.host": "http://localhost",
         "db.port": 3333
 	}
 }
 ```
### Options - forced types

You can force type on specified variables. Keys defined on settings will be always converted into
 specified typed value even if is not in right type. *For example* `db.port` will be always number, so 
 if you send value `"test"`, this variable will have value `NaN` because `"test"` is can not be converted
 to number. Same is with other types.

 ```javascript
 const opts = {
 	strings: ["db.host"],
 	numbers: ["db.port"],
 	booleans: ["db.test"],
    arrays: ["only"]
 }
 ```
 

### Donate me 😉

| QR | Paypal |
| ------ | ------ |
| ![](https://gitlab.com/uploads/-/system/personal_snippet/1929487/66399a49a06fa8eb9a0758b8673758c5/qr_sh.png) | [![](https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=DUT8W343NVGQQ&source=url) |


### License

 MIT - [MIT License](https://spdx.org/licenses/MIT.html)
 
 [logo]: https://gitlab.com/stanislavhacker/envfull/raw/master/logo.png
 [replit]: https://gitlab.com/stanislavhacker/envfull/raw/master/assets/replit.png
 
 [pipeline]: https://gitlab.com/stanislavhacker/envfull/badges/master/pipeline.svg
 [npm-version]: https://img.shields.io/npm/v/envfull.svg
 [npm-downloads]: https://img.shields.io/npm/dm/envfull.svg
 [license]: https://img.shields.io/npm/l/envfull.svg
 [node]: https://img.shields.io/node/v/envfull.svg
 [collaborators]: https://img.shields.io/npm/collaborators/envfull.svg
 
 [link-license]: https://gitlab.com/stanislavhacker/envfull/blob/master/LICENSE
 [link-npm]: https://www.npmjs.com/package/envfull
 [link-pipeline]: https://gitlab.com/stanislavhacker/envfull/pipelines
 [link-node]: https://nodejs.org/en/